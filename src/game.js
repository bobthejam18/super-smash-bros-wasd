export class Game {
    constructor() {
        this.context = this.getContext();
        this.fighters = [
            new Mario(104, STAGE_FLOOR, FighterDirection.RIGHT, 0),
            new Yoshi(280, STAGE_FLOOR, FighterDirection.LEFT, 1),
        ]
    
        this.fighters[0].opponent = this.fighters[1];
        this.fighters[1].opponent = this.fighters[0];

        this.entities = [
            new Stage(),
            ...this.fighters.map(fighter => new Shadow(fighter)),
            ...this.fighters,
            new FpsCounter(),
            new StatusBar(this.fighters),
        ];
    
        this.frameTime = {
            previous: 0,
            secondsPassed: 0,
        };
    }

    getContext() {
        const canvasEl = document.querySelector('canvas');
        const context = canvasEl.getContext('2d');

        context.imageSmoothingEnabled = false;

        return context;
    }

    update() {
        for (const entity of this.entities) {
            entity.update(this.frameTime, this.context);
        }
    }

    draw() {
        for (const entity of this.entities) {
            entity.draw(this.context);
        }
    }

    frame(time) {
        window.requestAnimationFrame(this.frame.bind(this));

        this.frameTime = {
            secondsPassed: (time - this.frameTime.previous) / 1000,
            previous = time,
        }

        pollGamepads();
        this.update();
        this.draw();
    }

    start() {
        regisiterKeyboardEvents();
        registerGamepadEvents();
        
        window.requestAnimationFrame(this.frame.bind(this));
    }
}
